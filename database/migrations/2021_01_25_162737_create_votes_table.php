<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->uuid('id');
            $table->enum('type', ['up', 'down'])->default('up');

            /**
             * Cannot use morphs() here because we are using uuids instead of the 
             * id()'s bigIncrements() type.
             */
            $table->string('voteable_type');
            $table->uuid('voteable_id');
            
            $table->uuid('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
