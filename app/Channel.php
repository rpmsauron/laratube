<?php

namespace App;

use App\Subscription;
use App\Video;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Channel extends Model implements HasMedia
{
    use HasMediaTrait;

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    
    public function registerMediaConversions(?Media $media = null)
    {
        $this->addMediaConversion('thumb')
                ->width(100)
                ->height(100);
    }

    public function image() :?string
    {
        if ($this->media->first()) {
            return $this->media->first()->getFullUrl('thumb');
        }

        return null;
    }
    
    public function editable() :bool
    {
        if (!auth()->check()) {
            return false;
        }

        return $this->user_id == auth()->user()->id;
    }

    public function subscriptions() :HasMany
    {
        return $this->hasMany(Subscription::class);
    }

    public function videos() :HasMany
    {
        return $this->hasMany(Video::class);
    }
}
