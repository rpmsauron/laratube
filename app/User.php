<?php

namespace App;

use App\Comment;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public $incrementing = false;

    /**
     * Required to make eager-load relations using UUIDs work, otherwise 
     * eager-load's second query "IN" operator will be in the example format:
     *
     * select * from `users` where `users`.`id` 
     * in (63a038d6-a838-4120-b250-398c06ad6e82, 9c93e0e0-e406-415a-b8dc-87d9a22ec5de, 
     * a0745926-1e37-4df1-8076-7fa4831e6fa8, bef5c0db-e79e-425a-9a44-60e00b3cdf5d, 
     * d1cb6015-ca95-49e5-b3e5-49a1e5a6aa3b)
     *
     * and since the binding values are not by default escaped with '' or "", the 
     * match will misfunction and will return integer-matching until the first 
     * alphabetic character of the uuid.
     * For example, the following would be an example of the second eager-load 
     * 'IN'-operation query:
     * select * from `users` where `users`.`id` in (18, 1, 3, 9699, 9886);
     * which is wrongly parsed, and thus as an integer-type field, it would match, 
     * for example, the following ids:
     * 3ab1cddd-2bc0-4879-9100-a08786aae420,
     * 3d7313ca-580c-4e1b-b944-a2c0949277d7,
     * 3f2142de-5a5c-42a3-9905-2a0caa6a2b63,
     * which is wrong.
     */
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();

        static::creating(
            function ($model)
            {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        );
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function channel() : HasOne
    {
        return $this->hasOne(Channel::class);
    }
    
    public function toggleVote($entity, $type)
    {
        $vote = $entity->votes
                ->where('user_id', $this->id)
                ->first();
        
        if ($vote) {
            $vote->update([
                'type' => $type
            ]);

            return $vote->refresh();
        } else {
            return $entity->votes()->create([
                'type'    => $type,
                'user_id' => $this->id
            ]);
        }
    }
    
    public function comments() :HasMany
    {
        return $this->hasMany(Comment::class);
    }
}
