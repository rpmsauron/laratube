<?php

namespace App;

use App\Channel;
use App\Comment;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Video extends Model
{
    protected $keyType = 'string';
    
    public function channel() :BelongsTo
    {
        return $this->belongsTo(Channel::class);
    }
    
    public function editable() :bool
    {
        return auth()->check() && $this->channel->user_id === auth()->user()->id;
    }

    public function votes() :MorphMany
    {
        return $this->morphMany(Vote::class, 'voteable');
    }
    
    public function comments() :HasMany
    {
        return $this->hasMany(Comment::class)
                ->whereNull('comment_id')
                ->orderBy('created_at', 'DESC');
    }

    
}
