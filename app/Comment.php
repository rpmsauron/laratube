<?php

namespace App;

use App\Video;
use App\Vote;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Comment extends Model
{
    protected $with = ['user', 'votes'];

    /**
     * Required to make eager-load relations using UUIDs work, otherwise 
     * eager-load's second query "IN" operator will be in the example format:
     *
     * select * from `users` where `users`.`id` 
     * in (63a038d6-a838-4120-b250-398c06ad6e82, 9c93e0e0-e406-415a-b8dc-87d9a22ec5de, 
     * a0745926-1e37-4df1-8076-7fa4831e6fa8, bef5c0db-e79e-425a-9a44-60e00b3cdf5d, 
     * d1cb6015-ca95-49e5-b3e5-49a1e5a6aa3b)
     *
     * and since the binding values are not by default escaped with '' or "", the 
     * match will misfunction and will return integer-matching until the first 
     * alphabetic character of the uuid.
     * For example, the following would be an example of the second eager-load 
     * 'IN'-operation query:
     * select * from `users` where `users`.`id` in (18, 1, 3, 9699, 9886);
     * which is wrongly parsed, and thus as an integer-type field, it would match, 
     * for example, the following ids:
     * 3ab1cddd-2bc0-4879-9100-a08786aae420,
     * 3d7313ca-580c-4e1b-b944-a2c0949277d7,
     * 3f2142de-5a5c-42a3-9905-2a0caa6a2b63,
     * which is wrong.
     *
     * Check research source from:
     * https://github.com/laravel/framework/issues/30702
     */
    protected $keyType = 'string';

    protected $appends = ['repliesCount'];

    public function video() :BelongsTo
    {
        return $this->belongsTo(Video::class);
    }

    public function getRepliesCountAttribute() :int
    {
        return $this->replies->count();
    }

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function replies() :HasMany
    {
        return $this->hasMany(Comment::class, 'comment_id')
                ->whereNotNull('comment_id');
    }

    public function votes() :MorphMany
    {
        return $this->morphMany(Vote::class, 'voteable');
    }
}
