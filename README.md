
# Laratube

## Intro
This project consists on a simple Laravel/PHP-built application with Vue.js to create a basic Video/Channel streaming platform, like Youtube.

It was developed on PHP version 7.4.5, with the Laravel framework version 7.14.1, over Mysql version 8.0.19.

## Background ##
This project is built for the  [Udemy](https://www.udemy.com/course/build-a-youtube-clone) course on [Laravel](https://laravel.com) instructed by [Kati Frantz](https://www.udemy.com/user/kati-frantz/).
It can be built locally on a localhost, requiring the 'ffmpeg' extension set up locally as well.

## Local install ##
#### Requirements: 
To use it locally, a webserver (I use [valet](https://laravel.com/docs/8.x/valet) for Mac to automate creation of localhosts with Nginx and PHP), PHP 7.4, and MySQL is required.
#### Install
To install it, get it first from the repository:
```
git clone git@bitbucket.org:rpmsauron/laratube.git
```
then switch into the project root directory and run:
```
composer install
```
to fetch the vendor folder with subsequent dependencies.

Copy the .env_example file at the root directory into a .env file and adapt the values as you need, paying special attention to the database configuration that must match your local database settings.

Run the migrations to ensure a basic database for usage:
```
php artisan migrate
```
or, with seed data also, for testing:
```
php artisan migrate --seed
```

Install 'ffmpeg' locally in order to be able to process the video conversions into streamable formats:

- On a MacOS:

You can  download it from [homebrew](https://brew.sh/), with:
```
brew install ffmpeg
```
In my case, as I could not install it with brew, due to possibly out-of-date OS, and I got an error "ffmpeg: no bottle available", so you can work around this by downloading it manually (alongside ffprobe, which is a dependency). For that, go to:
https://evermeet.cx/ffmpeg/
and click on “Download as ZIP” under the snapshot version for ffmpeg, and also for ffprobe.
After that, extract the .zip files.
Then you have to copy the ffmpeg and ffprobe files extracted into:
```
/usr/local/bin
```
by running (adapting the source paths to your own):
```
sudo cp ~/Downloads/ffmpeg /usr/local/bin/
sudo cp ~/Downloads/ffprobe /usr/local/bin/
```
and give them proper permissions:
```
sudo chmod +x /usr/local/bin/ffmpeg
sudo chmod +x /usr/local/bin/ffprobe
```

Now, simply running:
```
ffmpeg
```
should yield actual positive output:
```
ffmpeg version 4.3.1-tessus  https://evermeet.cx/ffmpeg/  Copyright (c) 2000-2020 the FFmpeg developers
  built with Apple clang version 11.0.0 (clang-1100.0.33.17)
  configuration: --cc=/usr/bin/clang --prefix=/opt/ffmpeg --extra-version=tessus --enable-avisynth --enable-fontconfig --enable-gpl --enable-libaom --enable-libass --enable-libbluray --enable-libdav1d --enable-libfreetype --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libmysofa --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libopenh264 --enable-libopenjpeg --enable-libopus --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvmaf --enable-libvo-amrwbenc --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxavs --enable-libxvid --enable-libzimg --enable-libzmq --enable-libzvbi --enable-version3 --pkg-config-flags=--static --disable-ffplay
  libavutil      56. 51.100 / 56. 51.100
  libavcodec     58. 91.100 / 58. 91.100
  libavformat    58. 45.100 / 58. 45.100
  libavdevice    58. 10.100 / 58. 10.100
  libavfilter     7. 85.100 /  7. 85.100
  libswscale      5.  7.100 /  5.  7.100
  libswresample   3.  7.100 /  3.  7.100
  libpostproc    55.  7.100 / 55.  7.100
Hyper fast Audio and Video encoder
usage: ffmpeg [options] [[infile options] -i infile]... {[outfile options] outfile}...

Use -h to get full help or, even better, run 'man ffmpeg'
```
and running:
```
ffprobe
```
should too:
```
ffprobe version N-100675-gf359575c0b-tessus  https://evermeet.cx/ffmpeg/  Copyright (c) 2007-2021 the FFmpeg developers
  built with Apple clang version 11.0.0 (clang-1100.0.33.17)
  configuration: --cc=/usr/bin/clang --prefix=/opt/ffmpeg --extra-version=tessus --enable-avisynth --enable-fontconfig --enable-gpl --enable-libaom --enable-libass --enable-libbluray --enable-libdav1d --enable-libfreetype --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libmysofa --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libopenh264 --enable-libopenjpeg --enable-libopus --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvmaf --enable-libvo-amrwbenc --enable-libvorbis --enable-libvpx --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxavs --enable-libxvid --enable-libzimg --enable-libzmq --enable-libzvbi --enable-version3 --pkg-config-flags=--static --disable-ffplay
  libavutil      56. 63.101 / 56. 63.101
  libavcodec     58.117.101 / 58.117.101
  libavformat    58. 65.101 / 58. 65.101
  libavdevice    58. 11.103 / 58. 11.103
  libavfilter     7. 96.100 /  7. 96.100
  libswscale      5.  8.100 /  5.  8.100
  libswresample   3.  8.100 /  3.  8.100
  libpostproc    55.  8.100 / 55.  8.100
Simple multimedia streams analyzer
usage: ffprobe [OPTIONS] [INPUT_FILE]

You have to specify one input file.
Use -h to get full help or, even better, run 'man ffprobe'.
```

## Running the application

To be able to process the videos, you have to have the 'queue worker' running, which can be executed from the project root, via terminal, as:
```
php artisan queue:work --sleep=0 --timeout 60000
```
where 60000 is the timeout period highly extended to be able to process large videos into streamable format.
You will see:
```
[2021-01-27 18:02:56][14] Processing: App\Jobs\Videos\ConvertForStreaming
[2021-01-27 18:03:13][14] Processed:  App\Jobs\Videos\ConvertForStreaming
```
entries for each video that is processing or is processed.

To access the application just access the hostname you have configured for it from the webserver.

## Domain
The following entities exist in the application:

___
#### Users

The users of the applications, with respective login, register, recover password functionalities. Upon login the User has a top bar with their name, clickable as dropdown menu with options to logout and visit their own channel.
The main content features a simple search functionality for videos and channel by all Users.

___
#### Channels

Each User has their own channel, created by default upon registering a User, which will host their uploaded videos.

___
#### Videos

The videos uploaded by the Users and processed into streamable format.

___
#### Comments

The videos can be commented on by any User, and these can be replied to as well.

___
#### Votes

Both videos and comments can be upvoted or downvoted by Users.

___
#### Subscriptions

Users can subscribe and unsubscribe to/from other Users' Channels.
___
#### Model

An extendable new Model named 'Model' extends Laravel's BaseModel by replacing the default auto-incrementing integer if field with UUID fields.


## Debug Notes
### Video processing
if for some reason the command running the process fails, check the Laravel logs (storage/logs/) and, somewhere in there, will be an output with the actual ffmpeg command that is run (the Laravel extension [pbmedia/laravel-ffmpeg](https://github.com/protonemedia/laravel-ffmpeg) simply runs the ffmepg extension itself from its path). Try to run it manually on the CLI. You will get an actual error output with the probable cause.
### Query logging
All queries executed are logged in DEBUG env into 
```
storage/logs/query.log
```

### Compile vue.js changes
To compile Vue.js components you need [npm](https://www.npmjs.com/), the JS package manager tool, which can be installed with:
```
npm install
```
Then to compile the assets and changes, you can either have the process running:
```
npm run watch
```
which will detect any changes instantly and generate a build, or manually run:
```
npm run dev
```
You will get an output like:
```
 DONE  Compiled successfully in 10945ms     5:49:50 PM

       Asset      Size   Chunks             Chunk Names
/css/app.css   179 KiB  /js/app  [emitted]  /js/app
  /js/app.js  1010 KiB  /js/app  [emitted]  /js/app
```

