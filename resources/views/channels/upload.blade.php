@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <channel-uploads :channel="{{ $channel }}" 
            inline-template>
            <div class="col-md-8">
                <div class="card p-3 d-flex justify-content-center align-items-center"
                    v-if='!selected'>
                    <svg onclick="document.getElementById('video-files').click()"
                        xmlns="http://www.w3.org/2000/svg" 
                        xmlns:xlink="http://www.w3.org/1999/xlink" 
                        xmlns:v="https://vecta.io/nano" 
                        viewBox="0 0 170 170"
                        width="100px"
                        height="100px">
                        <path xmlns="http://www.w3.org/2000/svg" 
                            d="M154.3 17.5a19.6 19.6 0 0 0-13.8-13.8C128.4.4 79.7.4 79.7.4S31 .5 18.9 3.8A19.6 19.6 0 0 0 5.1 17.6C1.44 39.1.02 71.86 5.2 92.5A19.6 19.6 0 0 0 19 106.3c12.1 3.3 60.8 3.3 60.8 3.3s48.7 0 60.8-3.3a19.6 19.6 0 0 0 13.8-13.8c3.86-21.53 5.05-54.27-.1-75z" 
                            fill="red"/>
                        <path xmlns="http://www.w3.org/2000/svg" 
                            fill="#fff" 
                            d="M64.2 78.4L104.6 55 64.2 31.6z"/>
                    </svg> 
                    
                    <input type="file" 
                        ref="videos"
                        id="video-files"
                        style="display: none;"
                        @change="upload"
                        multiple>
                    
                    <p class="text-center">Upload videos</p>
                </div> 
                
                <div class="card p-3"
                    v-else>
                    <div class="my-4" v-for="video in videos">
                        <div class="progress mb-3">
                            <div class="progress-bar progress-bar-striped progress-bar-animated"
                                role="progressbar"
                                :style="{ width: `${video.percentage || progress[video.name]}%` }" 
                                aria-valuenow="50"
                                aria-valuein="0"
                                aria-valuemax="100">
                                @{{ video.percentage ? video.percentage == 100 ? 'Processing Complete' : 'Processing' : 'Uploading' }}
                            </div>
                        </div>
                            
                        <div class="row">
                            <div class="col-md-4">
                                <div v-if="!video.thumbnail"
                                    class="d-flex justify-content-center align-items-center"
                                    style="height: 180px; color: white; font-size: 18px; background: #808080;">
                                         Loading thumbnail ...
                                </div>
                                <img v-else 
                                    :src="video.thumbnail"
                                    alt=""
                                    style="width: 100%;">
                            </div>
                            <div class="col-md-4">
                                <a v-if="video.percentage && video.percentage == 100"
                                    :href="`/videos/${video.id}`"
                                    target="_blank">
                                    @{{ video.title }}
                                </a>
                                <h4 v-else
                                    class="text-center">
                                    @{{ video.title || video.name }}
                                </h4>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </channel-uploads>
    </div>
</div>
@endsection
