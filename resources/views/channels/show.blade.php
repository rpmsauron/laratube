@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    {{ $channel->name }}
                    <a href="{{ route('channel.upload', $channel->id) }}">Upload videos</a>
                </div>

                <div class="card-body">
                    @if($channel->editable())
                        <form id="update-channel-form" 
                        action="{{ route('channels.update', $channel->id) }}" 
                        method="POST" 
                        enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                    @endif

                        <div class="form-group row justify-content-center">
                            <div class="channel-avatar">
                                
                                @if($channel->editable())
                                    <div class="channel-avatar-overlay"
                                        onclick="document.getElementById('image').click()">
                                        <svg xmlns="http://www.w3.org/2000/svg" 
                                             xmlns:xlink="http://www.w3.org/1999/xlink" 
                                             version="1.1" 
                                             id="Layer_1" 
                                             x="0px" 
                                             y="0px" 
                                             width="50px" 
                                             height="50px" 
                                             viewBox="0 0 512 512" 
                                             enable-background="new 0 0 512 512" 
                                             xml:space="preserve">
                                            <g fill="#fff">
                                                <circle cx="255.811" 
                                                    cy="285.309" 
                                                    r="75.217"/>
                                                <path d="M477,137H352.718L349,108c0-16.568-13.432-30-30-30H191c-16.568,0-30,13.432-30,30l-3.718,29H34   c-11.046,0-20,8.454-20,19.5v258c0,11.046,8.954,20.5,20,20.5h443c11.046,0,20-9.454,20-20.5v-258C497,145.454,488.046,137,477,137   z M255.595,408.562c-67.928,0-122.994-55.066-122.994-122.993c0-67.928,55.066-122.994,122.994-122.994   c67.928,0,122.994,55.066,122.994,122.994C378.589,353.495,323.523,408.562,255.595,408.562z M474,190H369v-31h105V190z"/>
                                            </g>
                                        </svg>
                                    </div>
                                @endif
                                
                                <img src="{{ $channel->image() }}" alt="">
                                
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <h4 class="text-center">
                                {{ $channel->name }}
                            </h4>
                            
                            <p class="text-center">
                                {{ $channel->description }}
                            </p>
                             
                            <div class="text-center">
                                <subscribe-button :channel="{{ $channel }}"
                                    :initial-subscriptions="{{ $channel->subscriptions }}"/>
                            </div>
                            
                        </div>
                        
                        @if($channel->editable())
                            <input id="image" 
                                type="file" 
                                name="image"
                                style="display: none;"
                                onchange="document.getElementById('update-channel-form').submit()">

                            <div class="form-group">
                                <label for="name" 
                                    class="form-control-label">
                                    Name
                                </label>
                                <input type="text"
                                    class="form-control"
                                    id="name"
                                    name="name"
                                    value="{{ $channel->name }}">
                            </div>

                            <div class="form-group">
                                <label for="description" 
                                    class="form-control-label">
                                    Description
                                </label>
                                <textarea class="form-control"
                                    id="description"
                                    name="description"
                                    rows="3"
                                    >{{ $channel->description }}</textarea>
                            </div>
                        
                            @if($errors->any())
                            <ul class="list-group mb-5">
                                @foreach($errors->all() as $error)
                                    <li class="text-danger list-group-item">
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                            @endif

                            <button class="btn btn-info"
                                type="submit">
                                Update channel
                            </button>
                        @endif

                    @if($channel->editable())
                        </form>
                    @endif
                </div>
            </div> 
            
            <div class="card">
                <div class="card-header">
                    Videos
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Views</th>
                            <th>Status</th>
                            <th></th>
                        </thead>

                        <tbody>
                            @foreach($videos as $video)
                                <tr>
                                    <td>
                                        <img src="{{ $video->thumbnail }}" 
                                            alt=""
                                            width="40px"
                                            height="40px">
                                    </td>
                                    <td>{{ $video->title }}</td>
                                    <td>{{ $video->views }}</td>
                                    <td>
                                        {{ $video->percentage === 100 ? "Live" : "Processing" }}
                                    </td>
                                    <td>
                                        @if($video->percentage === 100)
                                            <a href="{{ route('videos.show', $video->id) }}"
                                                class="btn btn-sm btn-info">View</a>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="row justify-content-center">
                        {{ $videos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
